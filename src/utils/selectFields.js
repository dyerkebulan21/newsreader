export const selectFields = ({id,title,time,url,by} = {}) => ( {
    id,
    title,
    time,
    url,
    by
})